const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const uuidv1 = require('uuid/v1');

var data = [];

const app = express();
app.use(bodyParser.json({ type: 'application/json' }));

app.use(cors({
  origin: 'http://localhost:3000',
  methods: ['GET', 'POST', 'PATCH', 'DELETE'],
}));

var ordersRouter = express.Router();
ordersRouter.get('/', handleGetAll);
ordersRouter.get('/:id', handleGet);
ordersRouter.post('/', handlePost);
ordersRouter.patch('/:id', handlePatch);
ordersRouter.delete('/:id', handleDelete);

app.use('/api/orders', ordersRouter);

app.use(function(req, res, next) {
  res.status(404);
  console.log('Not found URL: %s',req.url);
  res.send({ error: 'Not found' });
  return;
});

app.listen(8080, function(){
  console.log('Express server listening on port 8080');
});

function handleGetAll(req, res) {
  if (data) {
    return res.send({ data });
  } else {
    res.statusCode = 500;
    console.log('Error. 500.');
    return res.send({ error: 'Server error' });
  }
};

function handleGet(req, res) {
  try {
    if (data) {
      const order = data.filter(function(item) {
        return item.id === req.params.id;
      });
      if (order.length === 0)
        throw 'Order does not exists';
      return res.send({ data: order });
    } else {
      res.statusCode = 500;
      console.log('Error. 500.');
      return res.send({ error: 'Server error' });
    }
  } catch(error) {
    console.log('Error.', error);
    res.statusCode = 500;
    return res.send({ error });
  }
};

function handlePost(req, res) {
  var order = null;
  try {
    order = {
      id: uuidv1(),
      type: 'orders',
      attributes: {
        date: req.body.date,
        name: req.body.name,
        delivery: req.body.delivery,
        count: req.body.count,
        customer: req.body.customer,
        comment: req.body.comment,
      },
    };
    data.push(order);
  } catch (err) {
    console.log('Error while creating order. Err: ', err);
    res.statusCode = 500;
    res.send({ error: 'Validation error' });
    return;
  }

  return res.send({ status: 'OK', data: [order] });
};

function handlePatch(req, res) {
  try {
    var number;
    const patchedOrder = {
      id: req.params.id,
      type: 'orders',
      attributes: {
        date: req.body.date,
        name: req.body.name,
        delivery: req.body.delivery,
        count: req.body.count,
        customer: req.body.customer,
        comment: req.body.comment,
      },
    };

    data = data.map(function(item, index) {
      if (item.id === req.params.id) {
        number = index;
        return patchedOrder;
      }
      return item;
    });
    return res.send({ data: [data[number]] });
  } catch(err) {
    console.log('handlePatch error: ', err);
    res.statusCode = 500;
    res.send({ error: 'Validation error' });
  }
};

function handleDelete(req, res) {
  try {
    data = data.filter(function(item) {
      return item.id !== req.params.id;
    });
    return res.send(data);
  } catch (err) {
    console.log('Error while creating order. Err: ', err);
    res.statusCode = 500;
    res.message = err;
    res.send({ error: 'Validation error' });
    return;
  }
};
