# Тестовое задание

Front - React.Js

## Запуск фронта 
1. `npm i`
2. `npm run start`
Сервер поднимется на 3000 порту

## Запуск сервера
1. `node server`
Сервер поднимется на 8080 порту

## Роутинг

```
/ - страница всех заказов (OrdersPage)
/add - страница добавления заказа (OrderAddForm)
/edit/:id - страница редактирования заказа (OrderEditForm)
```

## API

```
/api/orders - список всех заказов (GET)
/api/orders - добавление заказа (POST)
/api/orders/:id - редактировать заказ (PATCH)
/api/orders/:id - удалить заказ (DELETE)
```
