import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import 'sanitize.css';

import 'global-styles';
import { getAsyncInjectors } from './utils/asyncInjectors';
import configureStore from './store';

import Header from 'components/Header';
import Loader from 'components/Loader';

const initialState = {};
const history = createBrowserHistory();
const store = configureStore(initialState, history);

const { injectSagas } = getAsyncInjectors(store);

function asyncComponent(componentName, sagas) {
  return class AsyncComponent extends React.Component {
    state = {
      Component: null,
    };

    componentWillMount() {
      if (!this.state.Component) {
        const importModules = Promise.all([
          sagas && import(`${componentName}/sagas`),
          import(`${componentName}`),
        ]);

        importModules.then(([sagas, Component]) => {
          if (sagas)
            injectSagas(sagas.default);

          this.setState({ Component: Component.default });
        });

        importModules.catch((err) => {
          console.error('Dynamic page loading failed', err);
        });
      }
    }
    render() {
      if (this.state.Component) {
        return <this.state.Component {...this.props} />;
      }
      return <Loader />;
    }
  }
};

const OrdersPage = asyncComponent('./containers/OrdersPage', './containers/OrdersPage/sagas');
const OrderAddForm = asyncComponent('./containers/OrderAddForm', './containers/OrderAddForm/sagas');
const OrderEditForm = asyncComponent('./containers/OrderEditForm');

const RootRoutes = () => (
  <React.Fragment>
    <Header />
    <Route path="/add" component={OrderAddForm} />
    <Route path="/edit/:id" component={OrderEditForm} />
    <Route path="/" exact component={OrdersPage} />
  </React.Fragment>
);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route path="/" component={RootRoutes} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
