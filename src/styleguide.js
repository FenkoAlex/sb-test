import { css } from 'styled-components';

export const COLORS = {
  athens: '#f2f4f6',
  trout: '#4b545b',
  fern: '#53b374',
  shuttle: '#5d676e',
  gallery: '#f0f0f0',
  cornflower: '#4f7bf7',
  silver: '#c9cbcd',
};

export const BREAKPOINTS = {
  tablet: '768px',
  desktop: '1024px',
};

export const TYPOGRAPHY = {
  body: css`
    font-family: 'SourceSans-Light', sans-serif;
    font-size: 14px;
    font-weight: 300;
  `,
  label: css`
    font-family: 'SourceSans-Regular', sans-serif;
    font-size: 14px;
    font-weight: 400;
  `,
};

export const Z_INDEXES = {
  dropdown: 100,
};
