import { injectGlobal } from 'styled-components';
import { COLORS, TYPOGRAPHY } from 'styleguide';

injectGlobal`
  /* SourceSans-300 - SourceSans-Light */
  @font-face {
    font-style: normal;
    font-family: 'SourceSans-Light';
    font-weight: 300;
    src:
      url('fonts/SourceSansPro-Light.wofff2') format('woff2'),
      url('fonts/SourceSansPro-Light.woff') format('woff');
  }

  /* SourceSans-400 - SourceSans-Regular */
  @font-face {
    font-style: normal;
    font-family: 'SourceSans-Regular';
    font-weight: 400;
    src:
      url('fonts/SourceSansPro-Regular.woff2') format('woff2'),
      url('fonts/SourceSansPro-Regular.woff') format('woff');
  }

  html {
    height: 100%;
    width: 100%;
  }

  body {
    height: 100%;
    width: 100%;
    background-color: ${COLORS.athens};
    color: ${COLORS.trout};
    ${TYPOGRAPHY.body}
  }
`;
