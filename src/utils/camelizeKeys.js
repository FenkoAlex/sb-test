import { camelCase, snakeCase, mapKeys } from 'lodash';

export default function camelizeKeys(object) {
  return mapKeys(object, (value, key) => camelCase(key));
}

function decamelizeKeys(object) {
  return mapKeys(object, (value, key) => snakeCase(key));
}

export {
  decamelizeKeys,
};
