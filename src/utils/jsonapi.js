import camelizeKeys from 'utils/camelizeKeys';

export function parseJsonApi(response, type) {
  return []
    .concat(Array.isArray(response.data) ? response.data : [response.data])
    .filter((item) => item.type === type)
    .reduce((previousValue, currentValue) => {
      const value = { ...previousValue };

      value[currentValue.id] = {
        id: currentValue.id,
        ...camelizeKeys(currentValue.attributes),
      };

      return value;
    }, {});
}
