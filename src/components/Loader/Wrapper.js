import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 25px auto;
  text-align: center;
`;

export default Wrapper;
