import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  0% {
    transform: rotate(360deg);
  }

  100% {
    transform: rotate(0deg);
  }
`;

const Svg = styled.svg`
  animation: ${spin} 1.5s infinite linear;
`;

export default Svg;
