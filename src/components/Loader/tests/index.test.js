import React from 'react';
import { shallow } from 'enzyme';

import Loader from '../';

describe('Loader', () => {
  it('should be a div', () => {
    expect(shallow(<Loader />).contains(
      <circle cx="50" cy="50" r="35" fill="none" stroke="#3566f0" strokeWidth="5" strokeDasharray="94 106" />
    )).toBe(true);
  });
});
