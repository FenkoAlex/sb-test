import React from 'react';
import Wrapper from './Wrapper';
import Svg from './Svg';

function Loader() {
  return (
    <Wrapper>
      <Svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <circle cx="50" cy="50" r="35" fill="none" stroke="#3566f0" strokeWidth="5" strokeDasharray="94 106" />
      </Svg>
    </Wrapper>
  );
}

export default Loader;
