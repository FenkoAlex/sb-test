import styled from 'styled-components';

const ErrorMessage = styled.p`
  width: 100%;
  text-align: center;
  color: red;
`;

export default ErrorMessage;
