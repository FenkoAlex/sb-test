import React from 'react';
import { shallow } from 'enzyme';

import Arrow from '../Arrow';

describe('<Arrow />', () => {
  it('should be svg', () => {
    const wrapper = shallow(<Arrow />);
    expect(wrapper.type()).toBe('svg');
  });
});
