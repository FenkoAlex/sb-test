import React from 'react';

function Arrow() {
  return (
    <svg width="8" height="4" xmlns="http://www.w3.org/2000/svg" >
      <defs>
        <path id="a" d="M4 4l4-4H0z"/>
      </defs>
      <g fill="none" fillRule="evenodd">
        <mask id="b" fill="#fff">
          <use xlinkHref="#a"/>
        </mask>
        <g mask="url(#b)" fill="currentColor">
          <path d="M0 0h8v4H0z"/>
        </g>
      </g>
    </svg>
  );
}

export default Arrow;
