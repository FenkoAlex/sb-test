import styled from 'styled-components';
import { rgba } from 'polished';

import { BREAKPOINTS } from 'styleguide';

const Wrapper = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  margin: auto;
  padding: 20px 25px;
  background: #fff;
  box-shadow: 0 2px 3px 0 ${rgba('#000', 0.2)};
  border-radius: 4px;

  @media (min-width: ${BREAKPOINTS.desktop}) {
    width: 400px;
    margin: 40px auto 50px;
  }
`;

export default Wrapper;
