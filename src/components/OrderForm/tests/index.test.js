import React from 'react';
import { shallow } from 'enzyme';

import OrderForm from '../';

describe('OrderForm', () => {
  it('should contains Дата заказа', () => {
    expect(shallow(<OrderForm />).contains('Дата заказа')).toBe(true);
  });

  it('should call handleInputChange correctly', () => {
    const wrapper = shallow(<OrderForm />);
    wrapper.setState({ isDropdownOpen: false });
    wrapper.instance().handleInputChange('inputType', {
      target: {
        value: 'inputValue',
      },
    });
    expect(wrapper.state('inputType')).toBe('inputValue');
  });

  describe('should call handleSubmit correctly', () => {
    it('and call onSubmit prop if state delivery is not empty', () => {
      const spy = jest.fn();
      const wrapper = shallow(<OrderForm onSubmit={spy} />);
      wrapper.setState({ delivery: 'delivery' });
      wrapper.instance().handleSubmit({ preventDefault: () => {} });
      expect(spy).toBeCalled();
    });

    it('and set requiredDelivery to true if delivery is empty', () => {
      const wrapper = shallow(<OrderForm />);
      wrapper.instance().handleSubmit({ preventDefault: () => {} });
      expect(wrapper.state('requiredDelivery')).toBe(true);
    });
  });

  it('should call handleDropdownOpen correctly and set isDropdownOpen to true', () => {
    const wrapper = shallow(<OrderForm />);
    wrapper.setState({ isDropdownOpen: false });
    wrapper.instance().handleDropdownOpen();
    expect(wrapper.state('isDropdownOpen')).toBe(true);
  });

  it('should call handleDropdownClose correctly and set isDropdownOpen to false', () => {
    const wrapper = shallow(<OrderForm />);
    wrapper.setState({ isDropdownOpen: true });
    wrapper.instance().handleDropdownClose({ preventDefault: () => {} });
    expect(wrapper.state('isDropdownOpen')).toBe(false);
  });

  it('should call handleDropdownItemClick correctly and set delivery state', () => {
    const wrapper = shallow(<OrderForm />);
    wrapper.setState({ isDropdownOpen: true });
    wrapper.instance().handleDropdownItemClick({
      preventDefault: () => {},
      target: {
        value: 'newValue',
      },
    });
    expect(wrapper.state('delivery')).toBe('newValue');
  });
});
