import styled from 'styled-components';

import { BREAKPOINTS } from 'styleguide';

const Label = styled.label`
  margin-bottom: 15px;
  font-size: 24px;

  @media (min-width: ${BREAKPOINTS.desktop}) {
    margin-bottom: 30px;
  }
`;

export default Label;
