import styled from 'styled-components';
import { rgba, darken } from 'polished';

import { BREAKPOINTS, COLORS, TYPOGRAPHY } from 'styleguide';

const Button = styled.button`
  height: 40px;
  margin-bottom: 15px;
  font-size: 24px;
  background-color: ${COLORS.fern};
  transition: background-color 0.3s;
  padding: 0;
  border: 0;
  border-radius: 4px;
  box-shadow: 0 5px 15px 0 ${rgba(COLORS.fern, 0.3)};
  cursor: pointer;
  color: #fff;
  ${TYPOGRAPHY.body}

  &:hover {
    background-color: ${darken(0.1, COLORS.fern)};
  }

  &:focus,
  &:active {
    outline: 2px solid ${rgba(COLORS.fern, 0.9)};
  }

  @media (min-width: ${BREAKPOINTS.desktop}) {
    margin-bottom: 30px;
  }
`;

export default Button;
