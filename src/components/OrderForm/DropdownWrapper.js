import styled from 'styled-components';

import { BREAKPOINTS } from 'styleguide';

const DropdownWrapper = styled.div`
  position: relative;
  margin-top: 5px;

  @media (min-width: ${BREAKPOINTS.desktop}) {
    margin-top: 15px;
  }
`;

export default DropdownWrapper;
