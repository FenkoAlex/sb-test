import styled from 'styled-components';
import { rgba } from 'polished';

import { BREAKPOINTS, COLORS, TYPOGRAPHY } from 'styleguide';

const Wrapper = styled.input`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding: 0 15px;
  border: 1px solid ${COLORS.silver};
  border-radius: 5px;
  ${TYPOGRAPHY.body}

  &:disabled {
    background-color: ${COLORS.gallery};
    color: ${rgba(COLORS.silver, 0.5)};
    cursor: default;
  }

  @media (min-width: ${BREAKPOINTS.desktop}) {
    margin-top: 15px;
  }
`;

export default Wrapper;
