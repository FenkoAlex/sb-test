import TextareaAutosize from 'react-textarea-autosize';
import { rgba } from 'polished';

import { COLORS } from 'styleguide';

import Input from './Input';

const TextareaStyled = Input.withComponent(TextareaAutosize).extend`
  padding-top: 10px;
  padding-bottom: 10px;

  &:disabled {
    background-color: ${COLORS.gallery};
    color: ${rgba(COLORS.silver, 0.5)};
    cursor: default;
  }
`;

export default TextareaStyled;
