import React from 'react';

import Dropdown from 'components/Dropdown';
import DropdownContent from 'components/Dropdown/Content';
import DropdownItem from 'components/Dropdown/Item';
import DropdownButton from 'components/Dropdown/DropdownButton';
import ArrowWrapper from 'components/Dropdown/ArrowWrapper';
import Arrow from 'components/icons/Arrow';
import ErrorMessage from 'components/ErrorMessage';

import Wrapper from './Wrapper';
import Input from './Input';
import TextareaStyled from './TextareaStyled';
import Label from './Label';
import Button from './Button';
import DropdownWrapper from './DropdownWrapper';
import ErrorMessageStyled from './ErrorMessageStyled';

const DELIVERY_TYPES = [
  'Самовывов',
  'Доставка в пределах МКАД',
  'Доставка за МКАД',
  'Доставка почтой',
];

export class OrderForm extends React.PureComponent {
  state = {
    date: this.props.date || '',
    name: this.props.name || '',
    delivery: this.props.delivery || '',
    count: this.props.count || '',
    customer: this.props.customer || '',
    comment: this.props.comment || '',
    requiredDelivery: false,
    isDropdownOpen: false,
  }

  componentWillReceiveProps({
    date = '',
    name = '',
    delivery = '',
    count = '',
    customer = '',
    comment = '',
  }) {
    if (this.props.formType === 'edit') {
      this.setState({
        date,
        name,
        delivery,
        count,
        customer,
        comment,
      });
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.delivery === '') {
      this.setState({ requiredDelivery: true });
      return;
    }
    this.props.onSubmit({
      date: this.state.date,
      name: this.state.name,
      delivery: this.state.delivery,
      count: this.state.count,
      customer: this.state.customer,
      comment: this.state.comment,
    });
    this.setState({
      date: '',
      name: '',
      delivery: '',
      count: '',
      customer: '',
      comment: '',
    });
  }

  handleDropdownOpen = () => {
    this.setState({ isDropdownOpen: true });
  }

  handleDropdownClose = (e) => {
    e.preventDefault();
    this.setState({ isDropdownOpen: false });
  }

  handleDropdownItemClick = (e) => {
    e.preventDefault();
    this.setState({
      delivery: e.target.value,
      isDropdownOpen: false,
      requiredDelivery: false,
    });
  }

  handleInputChange = (inputType, e) => {
    this.setState({ [inputType]: e.target.value });
  }

  inputBind = inputType =>
    this.handleInputChange.bind(this, inputType)

  render() {
    return (
      this.props.error
        ? <ErrorMessage role="alert">{this.props.errors || 'При выполнении операции произошла ошибка'}</ErrorMessage>
        : (
          <Wrapper
            onSubmit={this.handleSubmit}
            innerRef={el => this.form = el}
          >
            <Label>
              Дата заказа
              <Input
                value={this.state.date}
                required
                disabled={this.props.loading}
                onChange={this.inputBind('date')}
              />
            </Label>

            <Label>
              Наименование
              <Input
                value={this.state.name}
                required
                disabled={this.props.loading}
                onChange={this.inputBind('name')}
              />
            </Label>

            <Label>
              Способ доставки
              <DropdownWrapper>
                <DropdownButton
                  name="owner"
                  disabled={this.state.loading}
                  onClick={this.handleDropdownOpen}
                >
                  {this.state.delivery || 'Выберите способ доставки'}
                  <ArrowWrapper><Arrow /></ArrowWrapper>
                </DropdownButton>

                {this.state.isDropdownOpen &&
                  <Dropdown onClose={this.handleDropdownClose}>
                    <DropdownContent>
                      {DELIVERY_TYPES
                        .filter(item => item !== this.state.delivery)
                        .map((type) => (
                          <DropdownItem
                            key={type}
                            type="button"
                            name="owner"
                            value={type}
                            onClick={this.handleDropdownItemClick}
                          >
                            {type}
                          </DropdownItem>
                        )
                      )}
                    </DropdownContent>
                  </Dropdown>
                }
              </DropdownWrapper>
              {this.state.requiredDelivery &&
                <ErrorMessageStyled role="alert">Поле обязательно для заполнения</ErrorMessageStyled>
              }
            </Label>

            <Label>
              Количество единиц
              <Input
                type="number"
                value={this.state.count}
                required
                disabled={this.props.loading}
                onChange={this.inputBind('count')}
              />
            </Label>

            <Label>
              ФИО
              <Input
                value={this.state.customer}
                required
                disabled={this.props.loading}
                onChange={this.inputBind('customer')}
              />
            </Label>

            <Label>
              Комментарий
              <TextareaStyled
                minRows={3}
                maxRows={6}
                value={this.state.comment}
                required
                disabled={this.props.loading}
                onChange={this.inputBind('comment')}
              ></TextareaStyled>
            </Label>

            <Button disabled={this.props.loading}>
              {this.props.formType === 'edit'
                ? 'Редактировать'
                : 'Добавить'
              }
            </Button>
          </Wrapper>
        )
    );
  }
}

export default OrderForm;
