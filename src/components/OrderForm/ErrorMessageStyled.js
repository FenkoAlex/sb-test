import ErrorMessage from 'components/ErrorMessage';

const ErrorMessageStyled = ErrorMessage.extend`
  margin: 5px 0 0 0;
  font-size: 13px;
  text-align: left;
`;

export default ErrorMessageStyled;


