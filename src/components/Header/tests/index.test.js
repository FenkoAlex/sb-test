import React from 'react';
import { shallow } from 'enzyme';

import Header from '../';

describe('<Header />', () => {
  it('should contains Все заказы', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.contains('Все заказы')).toBe(true);
  });
});
