import styled from 'styled-components';
import { rgba } from 'polished';
import { NavLink } from 'react-router-dom';

import { COLORS } from 'styleguide';

const StyledLink = styled(NavLink)`
  color: ${COLORS.trout};
  text-decoration: none;
  opacity: 0.8;
  transition: color 0.3s, opacity 0.3s;

  &.active {
    color: ${COLORS.fern};
  }

  &:hover,
  &:active {
    opacity: 1;
  }

  &:focus {
    box-shadow: 0 2px 4px 0 ${rgba('#fff', 0.1)};
  }
`;

export default StyledLink;
