import styled from 'styled-components';
import { rgba } from 'polished';

import { BREAKPOINTS } from 'styleguide';

const Wrapper = styled.header`
  display: flex;
  justify-content: space-around;
  width: 100%;
  margin: auto;
  padding: 20px 25px;
  background: #fff;
  box-shadow: 0 2px 4px 0 ${rgba('#000', 0.2)};

  @media (min-width: ${BREAKPOINTS.desktop}) {
    width: 400px;
    border-radius: 0 0 4px 4px;
  }
`;

export default Wrapper;
