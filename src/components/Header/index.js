import React from 'react';

import Wrapper from './Wrapper';
import StyledLink from './StyledLink';

function Header() {
  return (
    <Wrapper>
      <StyledLink exact to='/'>
        Все заказы
      </StyledLink>

      <StyledLink to='/add'>
        Добавить заказ
      </StyledLink>
    </Wrapper>
  );
}

export default Header;
