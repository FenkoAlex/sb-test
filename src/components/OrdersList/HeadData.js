import styled from 'styled-components';

const HeadData = styled.th`
  width: 200px;
  padding: 15px 24px 10px;
  text-align: left;
`;

export default HeadData;
