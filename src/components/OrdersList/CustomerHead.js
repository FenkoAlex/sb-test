import HeadData from './HeadData';

const CustomerHead = HeadData.extend`
  width: 250px;
`;

export default CustomerHead;
