import React from 'react';
import { shallow } from 'enzyme';
import { List } from 'immutable';

import Order from 'containers/Order';
import OrdersList from '../';

describe('OrdersList', () => {
  it('should contains Заказчик', () => {
    expect(shallow(<OrdersList />).contains('Заказчик')).toBe(true);
  });

  it('should contains Order if ordersIds is not empty', () => {
    const wrapper = shallow(<OrdersList ordersIds={List(['1'])}/>);
    expect(wrapper.contains(<Order key="1" orderId="1" />)).toBe(true);
  });
});
