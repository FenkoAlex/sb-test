import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';

import Order from 'containers/Order';
import Row from 'containers/Order/Row';

import Wrapper from './Wrapper';
import Head from './Head';
import HeadData from './HeadData';
import CustomerHead from './CustomerHead';
import DetailsHead from './DetailsHead';

export class OrdersList extends React.PureComponent {
  render() {
    return (
      <Wrapper>
        <Head>
          <Row>
            <CustomerHead>Заказчик</CustomerHead>
            <DetailsHead>Детали заказа</DetailsHead>
            <HeadData>Действия</HeadData>
          </Row>
        </Head>

        <tbody>
          {this.props.ordersIds && this.props.ordersIds.map((id) => (
            <Order key={id} orderId={id} />
          ))}
        </tbody>
      </Wrapper>
    );
  }
}

OrdersList.propTypes = {
  ordersIds: PropTypes.instanceOf(List),
};

export default OrdersList;
