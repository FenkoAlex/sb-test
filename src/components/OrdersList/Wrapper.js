import styled from 'styled-components';

import { BREAKPOINTS } from 'styleguide';

const Wrapper = styled.table`
  width: 100%;
  margin-top: 20px;
  background: #fff;

  @media (min-width: ${BREAKPOINTS.desktop}) {
    margin-top: 0;
  }
`;

export default Wrapper;
