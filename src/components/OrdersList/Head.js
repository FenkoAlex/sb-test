import styled from 'styled-components';

import { COLORS } from 'styleguide';

const Head = styled.thead`
  color: ${COLORS.shuttle};
`;

export default Head;
