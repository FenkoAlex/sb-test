import styled from 'styled-components';
import { rgba } from 'polished';

import { COLORS, TYPOGRAPHY } from 'styleguide';

const DropdownButton = styled.button.attrs({
  type: 'button',
})`
  display: flex;
  align-items: center;
  width: 100%;
  height: 45px;
  padding: 0 15px;
  border: 1px solid ${COLORS.silver};
  border-radius: 4px;
  background: #fff;
  ${TYPOGRAPHY.body}
  cursor: pointer;

  &:disabled {
    background-color: ${COLORS.gallery};
    color: ${rgba(COLORS.silver, 0.5)};
    cursor: default;
  }
`;

export default DropdownButton;
