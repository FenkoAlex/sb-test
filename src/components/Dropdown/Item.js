import styled from 'styled-components';
import { rgba } from 'polished';

import { COLORS, TYPOGRAPHY } from 'styleguide';

const Item = styled.button.attrs({
  type: 'button',
  role: 'menuitem',
})`
  display: flex;
  width: calc(100% - 3px);
  height: 35px;
  align-items: center;
  margin: 1px;
  padding: 0 0 0 20px;
  border: 0;
  background-color: white;
  ${TYPOGRAPHY.body}
  color: ${COLORS.shark};
  cursor: pointer;
  transition: background-color 0.3s;

  &:hover {
    background-color: ${rgba(COLORS.cornflower, 0.05)};
  }
`;

export default Item;
