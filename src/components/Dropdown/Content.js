import styled from 'styled-components';
import { rgba } from 'polished';

import { COLORS } from 'styleguide';

const Content = styled.div`
  position: relative;
  width: 100%;
  max-height: 195px;
  padding-bottom: 1px;
  border: solid 0.5px ${rgba(COLORS.silver, 0.1)};
  border-radius: 3px;
  box-shadow: 0 5px 10px 1px ${rgba(COLORS.silver, 0.2)};
  background-color: white;
  overflow-y: auto;
  overflow-x: hidden;
`;

export default Content;
