import styled from 'styled-components';

const ArrowWrapper = styled.div`
  margin-left: auto;
`;

export default ArrowWrapper;
