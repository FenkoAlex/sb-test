import styled from 'styled-components';
import { rgba } from 'polished';

import { Z_INDEXES } from 'styleguide';

const Wrapper = styled.div`
  position: absolute;
  top: 100%;
  z-index: ${Z_INDEXES.dropdown};
  width: 100%;
  box-shadow: 0 2px 4px 0 ${rgba('#fff', 0.2)};
`;

export default Wrapper;
