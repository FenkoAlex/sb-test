import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './Wrapper';
import Overlay from './Overlay';

export class Dropdown extends React.PureComponent {
  render() {
    return (
      <Wrapper top={this.props.top} left={this.props.left}>
        <Overlay onClick={this.props.onClose} />

        {React.Children.toArray(this.props.children)}
      </Wrapper>
    );
  }
}

Dropdown.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func,
  top: PropTypes.string,
  left: PropTypes.string,
};

export default Dropdown;
