import React from 'react';
import { shallow } from 'enzyme';

import Dropdown from '../';
import Overlay from '../Overlay';

describe('<Dropdown />', () => {
  it('should contains Overlay', () => {
    const mock = () => {};
    const wrapper = shallow(<Dropdown onClose={mock} />);
    expect(wrapper.contains(<Overlay onClick={mock} />)).toBe(true);
  });
});
