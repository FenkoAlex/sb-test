import { fromJS, List } from 'immutable';
import { parseJsonApi } from 'utils/jsonapi';

import {
  LOAD_ORDERS,
  LOAD_ORDERS_SUCCESS,
  LOAD_ORDERS_ERROR,
} from 'containers/OrdersPage/constants';
import {
  ADD_ORDER,
  ADD_ORDER_SUCCESS,
  ADD_ORDER_ERROR,
} from 'containers/OrderAddForm/constants';
import {
  EDIT_ORDER_SUCCESS,
  GET_ORDER_SUCCESS,
} from 'containers/OrderEditForm/constants';

const initialState = fromJS({
  ids: List(),
  data: fromJS({}),
});

function ordersReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_ORDERS:
      return state
        .set('ids', List())
        .set('data', fromJS({}))
        .set('errorLoad', '')
        .set('loadingLoad', true);
    case LOAD_ORDERS_SUCCESS:
      return state
        .set('ids', fromJS(action.response.data.map((item) => item.id)))
        .set('data', fromJS(parseJsonApi(action.response, 'orders')))
        .set('loadingLoad', false);
    case LOAD_ORDERS_ERROR:
      return state
        .set('errorLoad', action.error.message)
        .set('loadingLoad', false);
    case ADD_ORDER:
      return state
        .set('errorAdd', '')
        .set('loadingAdd', true);
    case ADD_ORDER_SUCCESS:
    case GET_ORDER_SUCCESS:
    case EDIT_ORDER_SUCCESS:
      return state
        .set('data', state.get('data').mergeDeep(fromJS(parseJsonApi(action.response, 'orders'))))
        .set('loadingAdd', false);
    case ADD_ORDER_ERROR:
      return state
        .set('errorAdd', action.error.message)
        .set('loadingAdd', false);
    default:
      return state;
  }
}

export default ordersReducer;
