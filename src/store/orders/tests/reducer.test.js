import { fromJS } from 'immutable';
import ordersReducer from '../reducer';
import { parseJsonApi } from 'utils/jsonapi';

import {
  loadOrders,
  ordersLoaded,
  ordersLoadingError,
} from 'containers/OrdersPage/actions';
import {
  addOrder,
  addOrderSuccess,
  addOrderError,
} from 'containers/OrderAddForm/actions';

describe('ordersReducer', () => {
  let state;
  beforeEach(() => {
    state = fromJS({
      ids: [],
      data: fromJS({}),
    });
  });

  it('should return the initial state', () => {
    const initialState = state;
    expect(ordersReducer(undefined, {})).toEqual(initialState);
  });

  describe('load orders', () => {
    it('should handle the loadOrders action correctly', () => {
      const expectedResult = state
        .set('ids', fromJS([]))
        .set('data', fromJS({}))
        .set('errorLoad', '')
        .set('loadingLoad', true);;

      expect(ordersReducer(state, loadOrders())).toEqual(expectedResult);
    });

    it('should handle the ordersLoaded action correctly', () => {
      const response = { data: [{ id: '1', type: 'orders' }] };
      const expectedResult = state
        .set('ids', fromJS(response.data.map((item) => item.id)))
        .set('data', fromJS(parseJsonApi(response, 'orders')))
        .set('loadingLoad', false);

      expect(ordersReducer(state, ordersLoaded(response))).toEqual(expectedResult);
    });

    it('should handle the ordersLoadingError action correctly', () => {
      const errors = {
        message: 'error',
      };
      const expectedResult = state
        .set('errorLoad', errors.message)
        .set('loadingLoad', false);

      expect(ordersReducer(state, ordersLoadingError(errors))).toEqual(expectedResult);
    });
  });

  describe('add order', () => {
    it('should handle the addOrder action correctly', () => {
      const expectedResult = state
        .set('errorAdd', '')
        .set('loadingAdd', true);;

      expect(ordersReducer(state, addOrder())).toEqual(expectedResult);
    });

    it('should handle the addOrderSuccess action correctly', () => {
      const response = { data: [{ id: '1', type: 'orders' }] };
      const expectedResult = state
        .set('data', state.get('data').mergeDeep(fromJS(parseJsonApi(response, 'orders'))))
        .set('loadingAdd', false);

      expect(ordersReducer(state, addOrderSuccess(response))).toEqual(expectedResult);
    });

    it('should handle the addOrderError action correctly', () => {
      const errors = {
        message: 'error',
      };
      const expectedResult = state
        .set('errorAdd', errors.message)
        .set('loadingAdd', false);

      expect(ordersReducer(state, addOrderError(errors))).toEqual(expectedResult);
    });
  });
});
