import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import orders from './orders/reducer';

const rootReducer = combineReducers({
  orders,
  routing,
});

export default rootReducer;
