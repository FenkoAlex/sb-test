export const EDIT_ORDER = 'app/OrderEditForm/EDIT_ORDER';
export const EDIT_ORDER_SUCCESS = 'app/OrderEditForm/EDIT_ORDER_SUCCESS';
export const GET_ORDER_SUCCESS = 'app/OrderEditForm/GET_ORDER_SUCCESS';
