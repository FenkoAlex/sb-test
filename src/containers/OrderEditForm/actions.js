import {
  EDIT_ORDER_SUCCESS,
  GET_ORDER_SUCCESS,
} from './constants';

export function editOrderSuccess(response) {
  return {
    type: EDIT_ORDER_SUCCESS,
    response,
  };
}

export function getOrderSuccess(response) {
  return {
    type: GET_ORDER_SUCCESS,
    response,
  };
}
