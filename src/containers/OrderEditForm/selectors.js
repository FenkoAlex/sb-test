import { createSelector } from 'reselect';

const selectOrders = state => state.get('orders');

const makeSelectOrderById = (id) => createSelector(
  selectOrders,
  ordersState => ordersState.get('data').get(id),
);

export {
  selectOrders,
  makeSelectOrderById,
};
