import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router-dom';

import Orders from 'api/resources/orders';
import OrderForm from 'components/OrderForm';
import Loader from 'components/Loader';
import ErrorMessage from 'components/ErrorMessage';

import { makeSelectOrderById } from './selectors';
import { editOrderSuccess, getOrderSuccess } from './actions';

export class OrderEditForm extends React.PureComponent {
  state = {
    loading: true,
    error: '',
  }

  componentDidMount() {
    if (!this.props.order) {
      return Orders.get({
        orderId: this.props.match.params.id,
      }).then((response) => {
        this.setState({ loading: false });
        this.props.getOrderSuccess(response);
      }, (error) => {
      this.setState({
        loading: false,
        error: error.message,
      });
        console.log('error: ', error);
      });
    } else {
      this.setState({ loading: false });
    }
  }

  handleEditOrder = (body) => {
    this.setState({ loading: true });
    return Orders.patch({
      orderId: this.props.order.get('id'),
      body,
    }).then((response) => {
      this.setState({ loading: false });
      this.props.editOrderSuccess(response);
    }, (error) => {
      this.setState({ loading: false });
      console.log('error: ', error);
    });
  }

  render() {
    return (
      this.state.loading
        ? <Loader />
        : (
          this.props.order ?
            <OrderForm
              date={this.props.order.get('date')}
              name={this.props.order.get('name')}
              delivery={this.props.order.get('delivery')}
              count={this.props.order.get('count')}
              customer={this.props.order.get('customer')}
              comment={this.props.order.get('comment')}
              loading={this.state.loading}
              error={this.state.error}
              formType="edit"
              onSubmit={this.handleEditOrder}
            />
            : this.state.error
              && <ErrorMessage role="alert">{this.state.error}</ErrorMessage>
        )
    );
  }
}

OrderEditForm.propTypes = {
  addOrder: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => createStructuredSelector({
  order: makeSelectOrderById(ownProps.match.params.id),
});

export function mapDispatchToProps(dispatch) {
  return {
    editOrderSuccess: (response) => {
      dispatch(editOrderSuccess(response));
    },
    getOrderSuccess: (response) => {
      dispatch(getOrderSuccess(response));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(OrderEditForm));
