import React from 'react';

import Wrapper from './Wrapper';

class App extends React.PureComponent {
  render() {
    return (
      <Wrapper>
        {React.Children.toArray(this.props.children)}
      </Wrapper>
    );
  }
}

export default App;
