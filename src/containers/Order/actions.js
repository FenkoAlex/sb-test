import { DELETE_ORDER_SUCCESS } from './constants';

export function deleteOrderSuccess(response) {
  return {
    type: DELETE_ORDER_SUCCESS,
    response,
  };
}
