import styled from 'styled-components';

const Details = styled.td`
  padding: 15px 25px;
`;

export default Details;
