import styled from 'styled-components';

import { COLORS } from 'styleguide';

const Row = styled.tr`
  border-bottom: 1px solid ${COLORS.athens};
`;

export default Row;
