import styled from 'styled-components';
import { rgba, darken } from 'polished';

import { BREAKPOINTS, COLORS, TYPOGRAPHY } from 'styleguide';

const Button = styled.button`
  height: 40px;
  margin: 0 10px 15px 0;
  padding: 0 10px;
  border: 0;
  border-radius: 4px;
  background-color: ${COLORS.silver};
  box-shadow: 0 5px 15px 0 ${rgba(COLORS.silver, 0.3)};
  cursor: pointer;
  color: #fff;
  font-size: 24px;
  ${TYPOGRAPHY.body}
  transition: background-color 0.3s;

  &:hover {
    background-color: ${darken(0.1, COLORS.silver)};
  }

  &:focus,
  &:active {
    outline: 2px solid ${rgba(COLORS.silver, 0.9)};
  }

  &:disabled {
    background-color: ${darken(0.2, COLORS.silver)};
  }

  @media (min-width: ${BREAKPOINTS.desktop}) {
    margin-bottom: 30px;
  }
`;

export default Button;
