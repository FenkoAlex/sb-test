import styled from 'styled-components';

const OrderOption = styled.span`
  font-weight: 400;
`;

export default OrderOption;
