import { createSelector } from 'reselect';

const selectOrders = state => state.get('orders');

const makeSelectOrder = (id) => createSelector(
  selectOrders,
  ordersState => ordersState.get('data').get(id),
);

export {
  makeSelectOrder,
};
