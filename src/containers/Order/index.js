import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Map } from 'immutable';
import { withRouter } from 'react-router-dom';

import Orders from 'api/resources/orders';

import Row from './Row';
import TData from './TData';

import { makeSelectOrder } from './selectors';

import { deleteOrderSuccess } from './actions';

import Button from './Button';
import OrderOptionContainer from './OrderOptionContainer';
import OrderOption from './OrderOption';

export class Order extends React.PureComponent {
  handleDeleteClick = () => {
    return Orders.delete({
      orderId: this.props.order.get('id'),
    }).then((response) => {
      this.props.deleteOrderSuccess(response);
    }, (error) => {
      console.log('error: ', error);
    });
  }

  handleEditClick = () => {
    this.props.history.push(`/edit/${this.props.order.get('id')}`);
  }

  render() {
    return (
      <Row>
        <TData>{this.props.order.get('customer')}</TData>
        <TData>
          <OrderOptionContainer>
            <OrderOption>Наименование:</OrderOption> {this.props.order.get('name')}
          </OrderOptionContainer>

          <OrderOptionContainer>
            <OrderOption>Способ доставки:</OrderOption> {this.props.order.get('delivery')}
          </OrderOptionContainer>

          <OrderOptionContainer>
            <OrderOption>Количество:</OrderOption> {this.props.order.get('count')}
          </OrderOptionContainer>

          <OrderOptionContainer>
            <OrderOption>Комментарий:</OrderOption> {this.props.order.get('comment')}
          </OrderOptionContainer>
        </TData>
        <TData>
          <Button
            type="button"
            onClick={this.handleEditClick}
          >
            Редактировать
          </Button>
          <Button
            type="button"
            onClick={this.handleDeleteClick}
          >
            Удалить
          </Button>
        </TData>
      </Row>
    );
  }
}

Order.propTypes = {
  order: PropTypes.instanceOf(Map),
  orderId: PropTypes.string,
  deleteOrderSuccess: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => createStructuredSelector({
  order: makeSelectOrder(ownProps.orderId),
});

export function mapDispatchToProps(dispatch) {
  return {
    deleteOrderSuccess: (response) => {
      dispatch(deleteOrderSuccess(response));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Order));
