import { deleteOrderSuccess } from '../actions';
import { DELETE_ORDER_SUCCESS } from '../constants';

describe('Order actions', () => {
  describe('deleteOrderSuccess', () => {
    it('should return the correct type and the passed response', () => {
      const response = 'response';
      const expected = {
        type: DELETE_ORDER_SUCCESS,
        response,
      };

      expect(deleteOrderSuccess(response)).toEqual(expected);
    });
  });
});
