import { fromJS } from 'immutable';

import { makeSelectOrder } from '../selectors';

describe('makeSelectOrder', () => {
  const selector = makeSelectOrder('id1');
  it('should select the order by id', () => {
    const order = fromJS({ name: '1' });

    const state = order;

    const mockedState = fromJS({
      orders: {
        data: {
          id1: order,
        },
      },
    });

    expect(selector(mockedState)).toEqual(state);
  });
});
