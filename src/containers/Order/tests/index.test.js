import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';

import Orders from 'api/resources/orders';

import { Order, mapDispatchToProps } from '../';

import { deleteOrderSuccess } from '../actions';

describe('Order', () => {
  let requiredProps = null;

  beforeEach(() => {
    requiredProps = {
      order: fromJS({ id: 1 }),
    };
  });

  it('should contains Редактировать', () => {
    expect(shallow(<Order {...requiredProps} />).contains('Редактировать')).toBe(true);
  });

  it('should call handleDeleteClick correctly with success response', () => {
    const spy = jest.fn();
    const wrapper = shallow(<Order {...requiredProps} deleteOrderSuccess={spy} />);
    const response = {
      data: {
        attributes: {
          entry: 'foo',
        },
      },
    };
    Orders.delete = jest.fn().mockReturnValue(Promise.resolve(response));
    wrapper
      .instance()
      .handleDeleteClick()
      .then(() => {
        expect(spy).toBeCalledWith(response);
      });
  });

  describe('mapDispatchToProps', () => {
    describe('deleteOrderSuccess', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.deleteOrderSuccess).toBeDefined();
      });

      it('should dispatch deleteOrderSuccess when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const response = {
          data: {
            id: 'id',
            type: 'orders',
          },
        };
        result.deleteOrderSuccess(response);
        expect(dispatch).toHaveBeenCalledWith(deleteOrderSuccess(response));
      });
    });
  });
});
