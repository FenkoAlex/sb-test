import styled from 'styled-components';

import { BREAKPOINTS } from 'styleguide';

const OrderOptionContainer = styled.div`
  margin: 10px 0;

  @media (min-width: ${BREAKPOINTS.desktop}) {
    margin-top: 15px;
  }
`;

export default OrderOptionContainer;
