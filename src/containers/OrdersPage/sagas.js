import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';

import Orders from 'api/resources/orders';

import { DELETE_ORDER_SUCCESS } from 'containers/Order/constants';

import { LOAD_ORDERS } from './constants';

import {
  ordersLoaded,
  ordersLoadingError,
} from './actions';

export function* getOrders(action) {
  try {
    const response = yield call(Orders.getAll, action.payload);
    yield put(ordersLoaded(response));
  } catch (err) {
    const errors = err;
    yield put(ordersLoadingError(errors));
  }
}

export function* getOrdersWatcher() {
  const watcher = yield takeLatest(LOAD_ORDERS, getOrders);

  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

export function* deleteOrderWatcher() {
  const watcher = yield takeLatest(DELETE_ORDER_SUCCESS, getOrders);

  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

export default [
  getOrdersWatcher,
  deleteOrderWatcher,
];
