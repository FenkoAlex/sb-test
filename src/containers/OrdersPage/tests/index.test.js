import React from 'react';
import { shallow } from 'enzyme';
import { List } from 'immutable';

import OrdersList from 'components/OrdersList';

import { OrdersPage, mapDispatchToProps } from '../';
import { loadOrders } from '../actions';

describe('OrdersPage', () => {
  it('should contains <OrdersList /> if ordersIds props is not empty', () => {
    const ordersIds = List(['1']);
    const wrapper = shallow(<OrdersPage ordersIds={ordersIds} />);
    expect(wrapper.contains(<OrdersList ordersIds={ordersIds} />)).toBe(true);
  });

  it('should contains Список заказов пуст if ordersIds.size === 0', () => {
    const ordersIds = List([]);
    const wrapper = shallow(<OrdersPage ordersIds={ordersIds} />);
    expect(wrapper.contains('Список заказов пуст')).toBe(true);
  });

  it('should contains error message if errors is exists', () => {
    const wrapper = shallow(<OrdersPage errors="error" />);
    expect(wrapper.contains('error')).toBe(true);
  });

  it('should call componentDidMount and call loadOrders props', () => {
    const spy = jest.fn();
    const wrapper = shallow(<OrdersPage loadOrders={spy} />);
    wrapper.instance().componentDidMount();
    expect(spy).toBeCalled();
  });

  describe('mapDispatchToProps', () => {
    describe('loadOrders', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.loadOrders).toBeDefined();
      });

      it('should dispatch loadOrders when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.loadOrders();
        expect(dispatch).toHaveBeenCalledWith(loadOrders());
      });
    });
  });
});
