import { fromJS, List } from 'immutable';

import {
  selectOrders,
  makeSelectLoading,
  makeSelectErrors,
  makeSelectOrdersListId
} from '../selectors';

describe('selectOrders', () => {
  it('should select the orders state', () => {
    const fixture = 'orders';

    const mockedState = fromJS({
      orders: fixture,
    });

    expect(selectOrders(mockedState)).toEqual(fixture);
  });
});

describe('makeSelectLoading', () => {
  const selector = makeSelectLoading();
  it('should select the loading state', () => {
    const fixture = true;

    const mockedState = fromJS({
      orders: {
        loading: fixture,
      },
    });

    expect(selector(mockedState)).toEqual(fixture);
  });
});

describe('makeSelectErrors', () => {
  const selector = makeSelectErrors();
  it('should select the errors state', () => {
    const fixture = 'error';

    const mockedState = fromJS({
      orders: {
        errors: fixture,
      },
    });

    expect(selector(mockedState)).toEqual(fixture);
  });
});

describe('makeSelectOrdersListId', () => {
  const selector = makeSelectOrdersListId();
  it('should select the ids', () => {
    const fixture = List(['1']);

    const mockedState = fromJS({
      orders: {
        ids: fixture,
      },
    });

    expect(selector(mockedState)).toEqual(fixture);
  });
});
