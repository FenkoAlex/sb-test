import {
  loadOrders,
  ordersLoaded,
  ordersLoadingError,
} from '../actions';

import {
  LOAD_ORDERS,
  LOAD_ORDERS_SUCCESS,
  LOAD_ORDERS_ERROR,
} from '../constants';

describe('Order actions', () => {
  describe('loadOrders', () => {
    it('should return the correct type', () => {
      const expected = { type: LOAD_ORDERS };

      expect(loadOrders()).toEqual(expected);
    });
  });

  describe('ordersLoaded', () => {
    it('should return the correct type and the passed response', () => {
      const response = 'response';
      const expected = {
        type: LOAD_ORDERS_SUCCESS,
        response,
      };

      expect(ordersLoaded(response)).toEqual(expected);
    });
  });

  describe('ordersLoadingError', () => {
    it('should return the correct type and the passed error', () => {
      const error = 'error';
      const expected = {
        type: LOAD_ORDERS_ERROR,
        error,
      };

      expect(ordersLoadingError(error)).toEqual(expected);
    });
  });
});
