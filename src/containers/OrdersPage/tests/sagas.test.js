import { take, put, cancel, takeLatest } from 'redux-saga/effects';
import { createMockTask } from 'redux-saga/lib/utils';
import { LOCATION_CHANGE } from 'react-router-redux';

import { DELETE_ORDER_SUCCESS } from 'containers/Order/constants';

import { LOAD_ORDERS } from '../constants';

import {
  ordersLoaded,
  ordersLoadingError,
} from '../actions';

import {
  getOrders,
  getOrdersWatcher,
  deleteOrderWatcher,
} from '../sagas';

describe('getOrders Saga', () => {
  const action = {
    payload: 'payload',
  };

  let getOrdersGenerator;

  // We have to test twice, once for a successful load and once for an unsuccessful one
  // so we do all the stuff that happens beforehand automatically in the beforeEach
  beforeEach(() => {
    getOrdersGenerator = getOrders(action);

    const callDescriptor = getOrdersGenerator.next().value;
    expect(callDescriptor).toMatchSnapshot();
  });

  it('should dispatch the ordersLoaded action if it requests the data successfully', () => {
    const order = {};

    const putDescriptor = getOrdersGenerator.next(order).value;
    expect(putDescriptor).toEqual(put(ordersLoaded(order)));
  });

  it('should call the ordersLoadingError action if the response errors', () => {
    const response = 'Some error';
    const errorDescriptor = getOrdersGenerator.throw(response);
    expect(errorDescriptor.value).toEqual(put(ordersLoadingError(response)));
  });
});

describe('getOrdersWatcher Saga', () => {
  const ordersDataGenerator = getOrdersWatcher();
  const mockedTask = createMockTask();

  it('should start task to watch for LOAD_ORDERS action', () => {
    const takeLatestDescriptor = ordersDataGenerator.next().value;
    expect(takeLatestDescriptor).toEqual(takeLatest(LOAD_ORDERS, getOrders));
  });

  it('should yield until LOCATION_CHANGE action', () => {
    const takeDescriptor = ordersDataGenerator.next(mockedTask).value;
    expect(takeDescriptor).toEqual(take(LOCATION_CHANGE));
  });

  it('should cancel the forked task when LOCATION_CHANGE happens', () => {
    const cancelDescriptor = ordersDataGenerator.next().value;
    expect(cancelDescriptor).toEqual(cancel(mockedTask));
  });
});

describe('deleteOrderWatcher Saga', () => {
  const ordersDataGenerator = deleteOrderWatcher();
  const mockedTask = createMockTask();

  it('should start task to watch for LOAD_ORDERS action', () => {
    const takeLatestDescriptor = ordersDataGenerator.next().value;
    expect(takeLatestDescriptor).toEqual(takeLatest(DELETE_ORDER_SUCCESS, getOrders));
  });

  it('should yield until LOCATION_CHANGE action', () => {
    const takeDescriptor = ordersDataGenerator.next(mockedTask).value;
    expect(takeDescriptor).toEqual(take(LOCATION_CHANGE));
  });

  it('should cancel the forked task when LOCATION_CHANGE happens', () => {
    const cancelDescriptor = ordersDataGenerator.next().value;
    expect(cancelDescriptor).toEqual(cancel(mockedTask));
  });
});
