import { createSelector } from 'reselect';

const selectOrders = state => state.get('orders');

const makeSelectLoading = () => createSelector(
  selectOrders,
  ordersState => ordersState.get('loading'),
);

const makeSelectErrors = () => createSelector(
  selectOrders,
  ordersState => ordersState.get('errors'),
);

const makeSelectOrdersListId = () => createSelector(
  selectOrders,
  ordersState =>
    ordersState.get('ids'),
);

export {
  selectOrders,
  makeSelectLoading,
  makeSelectErrors,
  makeSelectOrdersListId,
};
