import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List } from 'immutable';
import { createStructuredSelector } from 'reselect';

import ErrorMessage from 'components/ErrorMessage';
import OrdersList from 'components/OrdersList';
import Loader from 'components/Loader';

import Wrapper from './Wrapper';

import {
  makeSelectOrdersListId,
  makeSelectLoading,
  makeSelectErrors,
} from './selectors';

import { loadOrders } from './actions';

export class OrdersPage extends React.PureComponent {
  componentDidMount() {
    this.props.loadOrders();
  }

  render() {
    return (
      <Wrapper>
        {this.props.ordersIds && <OrdersList ordersIds={this.props.ordersIds} />}

        {this.props.loading && <Loader />}

        {!this.props.loading && this.props.errors &&
          <ErrorMessage role="alert">{this.props.errors}</ErrorMessage>
        }

        {this.props.ordersIds &&
          this.props.ordersIds.size === 0 &&
          !this.props.errors &&
          !this.props.loading &&
          <ErrorMessage role="alert">Список заказов пуст</ErrorMessage>
        }
      </Wrapper>
    );
  }
}

OrdersPage.propTypes = {
  loadOrders: PropTypes.func,
  loading: PropTypes.bool,
  errors: PropTypes.string,
  ordersIds: PropTypes.instanceOf(List),
};

const mapStateToProps = createStructuredSelector({
  ordersIds: makeSelectOrdersListId(),
  loading: makeSelectLoading(),
  errors: makeSelectErrors(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loadOrders: () => {
      dispatch(loadOrders());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrdersPage);
