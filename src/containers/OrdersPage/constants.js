export const LOAD_ORDERS = 'app/OrdersPage/LOAD_ORDERS';
export const LOAD_ORDERS_SUCCESS = 'app/OrdersPage/LOAD_ORDERS_SUCCESS';
export const LOAD_ORDERS_ERROR = 'app/OrdersPage/LOAD_ORDERS_ERROR';
