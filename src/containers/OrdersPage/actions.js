import {
  LOAD_ORDERS,
  LOAD_ORDERS_SUCCESS,
  LOAD_ORDERS_ERROR,
} from './constants';

export function loadOrders() {
  return {
    type: LOAD_ORDERS,
  };
}

export function ordersLoaded(response) {
  return {
    type: LOAD_ORDERS_SUCCESS,
    response,
  };
}

export function ordersLoadingError(error) {
  return {
    type: LOAD_ORDERS_ERROR,
    error,
  };
}
