import styled from 'styled-components';

import { BREAKPOINTS } from 'styleguide';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;

  @media (min-width: ${BREAKPOINTS.desktop}) {
    width: 800px;
    margin: 40px auto 0;
    border-radius: 5px;
    background-color: #fff;
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  }
`;

export default Wrapper;
