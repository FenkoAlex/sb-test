import { createSelector } from 'reselect';

const selectOrders = state => state.get('orders');

const makeSelectLoadingAdd = () => createSelector(
  selectOrders,
  ordersState => ordersState.get('loadingAdd'),
);

const makeSelectErrorAdd = () => createSelector(
  selectOrders,
  ordersState => ordersState.get('errorAdd'),
);

export {
  selectOrders,
  makeSelectLoadingAdd,
  makeSelectErrorAdd,
};
