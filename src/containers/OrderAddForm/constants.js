export const ADD_ORDER = 'app/OrderAddForm/ADD_ORDER';
export const ADD_ORDER_SUCCESS = 'app/OrderAddForm/ADD_ORDER_SUCCESS';
export const ADD_ORDER_ERROR = 'app/OrderAddForm/ADD_ORDER_ERROR';
