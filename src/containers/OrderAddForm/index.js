import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import OrderForm from 'components/OrderForm';

import {
  makeSelectLoadingAdd,
  makeSelectErrorAdd,
} from './selectors';

import { addOrder } from './actions';

export class OrderAddForm extends React.PureComponent {
  handleAddOrder = (body) => {
    this.props.addOrder({ body });
  }

  render() {
    return (
      <OrderForm
        loading={this.props.loading}
        error={this.props.error}
        formType="add"
        onInputChange={this.handleInputChange}
        onSubmit={this.handleAddOrder}
      />
    );
  }
}

OrderAddForm.propTypes = {
  addOrder: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoadingAdd(),
  error: makeSelectErrorAdd(),
});

function mapDispatchToProps(dispatch) {
  return {
    addOrder: (response) => {
      dispatch(addOrder(response));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderAddForm);
