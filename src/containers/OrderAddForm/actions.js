import {
  ADD_ORDER,
  ADD_ORDER_SUCCESS,
  ADD_ORDER_ERROR,
} from './constants';

export function addOrder(payload) {
  return {
    type: ADD_ORDER,
    payload,
  };
}

export function addOrderSuccess(response) {
  return {
    type: ADD_ORDER_SUCCESS,
    response,
  };
}

export function addOrderError(error) {
  return {
    type: ADD_ORDER_ERROR,
    error,
  };
}
