import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';

import Orders from 'api/resources/orders';

import { ADD_ORDER } from './constants';

import {
  addOrderSuccess,
  addOrderError,
} from './actions';

export function* addOrder(action) {
  try {
    const response = yield call(Orders.add, action.payload);
    yield put(addOrderSuccess(response));
  } catch (err) {
    const errors = err;
    yield put(addOrderError(errors));
  }
}

export function* addOrderWatcher() {
  const watcher = yield takeLatest(ADD_ORDER, addOrder);

  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

export default [
  addOrderWatcher,
];
