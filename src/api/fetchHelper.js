import request from 'utils/request';

export function fetchGET(url) {
  const options = {
    method: 'GET',
    headers: {
      accept: 'application/json',
    },
  };

  return request(url, options);
}

export function fetchPATCH(url, body) {
  const options = {
    method: 'PATCH',
    headers: {
      'content-type': 'application/json',
      accept: 'application/json',
    },
    body: JSON.stringify(body),
  };

  return request(url, options);
}

export function fetchPOST(url, body) {
  const options = {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
      accept: 'application/json',
    },
    body: JSON.stringify(body),
  };

  return request(url, options);
}

export function fetchDELETE(url) {
  const options = {
    method: 'DELETE',
    headers: {
      'content-type': 'application/json',
      accept: 'application/json',
    },
  };

  return request(url, options);
}
