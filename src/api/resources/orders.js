import {
  fetchGET,
  fetchPOST,
  fetchPATCH,
  fetchDELETE,
} from 'api/fetchHelper';

const API_URL = 'http://localhost:8080/api';

const Orders = {
  getAll: () => {
    const url = `${API_URL}/orders`;
    return fetchGET(url);
  },
  get: ({ orderId }) => {
    if (!orderId) throw new Error('orderId is not defined');
    const url = `${API_URL}/orders/${orderId}`;
    return fetchGET(url);
  },
  add: ({ body }) => {
    const url = `${API_URL}/orders/`;
    return fetchPOST(url, body);
  },
  patch: ({ orderId, body }) => {
    if (!orderId) throw new Error('orderId is not defined');
    const url = `${API_URL}/orders/${orderId}`;
    return fetchPATCH(url, body);
  },
  delete: ({ orderId }) => {
    if (!orderId) throw new Error('orderId is not defined');
    const url = `${API_URL}/orders/${orderId}`;
    return fetchDELETE(url);
  },
};

export default Orders;
